using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static UnityEvent<int> OnPlateBroken = new UnityEvent<int>();
    public static UnityEvent<int> OnSpinPlate = new UnityEvent<int>();
    public static UnityEvent<int> OnResetPlateTime = new UnityEvent<int>();
    public static UnityEvent OnStartGame = new UnityEvent();
    public static UnityEvent OneUp = new UnityEvent();
    
    public Plate platePrefab;
    public GameObject heartPrefab;
    
    public int hp;
    public int maxHp = 8;
    public float timeToSpawn;

    public Transform scoreTransform;
    public Transform playerStartPosition;
    public Transform heartsGrid;
    public Transform plateTransform;
    public List<Transform> spawnPoints = new List<Transform>();
    public List<Transform> spinPoints = new List<Transform>();
    public PlayerController player;

    private static List<Transform> activeSpinPoints = new List<Transform>();
    
    private List<int> activatedSpawns = new List<int>();
    private static Dictionary<int, Plate> activePlates = new Dictionary<int, Plate>();
    
    
    private float lastSpawnTime;
    private int currentHp;
    
    private bool gameStarted;

    private bool spawning;

    private void Start()
    {
        OnPlateBroken.AddListener(BreakPlate);
        OnSpinPlate.AddListener(SpinPlate);
        OneUp.AddListener(AddOneHp);
        OnResetPlateTime.AddListener(ResetPlateTime);
        OnStartGame.AddListener(StartGame);
        
        foreach (var spinPoint in spinPoints)
            activeSpinPoints.Add(spinPoint);
        
    }
    
    private void Update()
    {
        if (!gameStarted)
        {
            if (Input.GetButton("Submit"))
            {
                UIEvents.OnStartGame.Invoke();
            }
            return;
        }

        if (!gameStarted) return;
        if (Input.GetButton("Cancel"))
        {
            GameOver();
            UIEvents.OnGameOver.Invoke();
        }

        if (Time.time - lastSpawnTime >= timeToSpawn && !spawning)
            SpawnPlate();
    }

    public void StartGame()
    {
        gameStarted = true;
        for (int i = 0; i < hp; i++)
            Instantiate(heartPrefab, heartsGrid);
        currentHp = hp;
        player.transform.position = playerStartPosition.position;
        SpawnPlate();
        scoreTransform.gameObject.SetActive(true);
        player.IsPlaying(true);
        player.StopHurt();
    }

    private void GameOver()
    {
        gameStarted = false;
        player.IsPlaying(false);
        player.OnHurt();

        DestroyInstances();
        
        activatedSpawns.Clear();
        scoreTransform.gameObject.SetActive(false);
    }

    private void DestroyInstances()
    {
        foreach (Transform plate in plateTransform)
            Destroy(plate.gameObject);
        foreach (Transform heart in heartsGrid)
            Destroy(heart.gameObject);
        foreach (var brokenPlate in FindObjectsOfType<BrokenPlate>())
            Destroy(brokenPlate.gameObject);
    }
    
    private void SpawnPlate()
    {
        if (IsSpawnSkipped()) return;

        int index = PickRandomIndex();

        var plate = InstantiatePlate(index);
        
        lastSpawnTime = Time.time;
        activatedSpawns.Add(index);
        activePlates[index] = plate;
    }

    private bool IsSpawnSkipped()
    {
        if (activatedSpawns.Count < spawnPoints.Count) return false;
        lastSpawnTime = Time.time;
        return true;
    }

    private int PickRandomIndex()
    {
        int index = Random.Range(0, spawnPoints.Count);
        while(activatedSpawns.Contains(index)) index = Random.Range(0, spawnPoints.Count);
        return index;
    }

    private Plate InstantiatePlate(int index)
    {
        var plate = Instantiate(platePrefab, spawnPoints[index].position, spawnPoints[index].rotation);
        plate.index = index;
        plate.transform.SetParent(plateTransform);
        return plate;
    }
    
    private static void ResetPlateTime(int index)
    {
        if (!activePlates.TryGetValue(index, out var plate)) return;
        if(plate)
            plate.SpinPlate();
    }
    
    private static void SpinPlate(int index)
    {
        if (!activePlates.TryGetValue(index, out var plate)) return;
        if(plate)
            plate.SpinUp();
    }

    public static bool IsPlateActive(int index)
    {
        return activePlates.TryGetValue(index, out var plate) 
               && plate && !plate.coll.isTrigger && plate.stage > Stage.Dropped && plate.stage < Stage.Fell;
    }
    
    private void BreakPlate(int index)
    {
        if (currentHp == 0)
        {
            UIEvents.OnGameOver.Invoke();
            GameOver();
            return;
        }
        currentHp--;
        Destroy(heartsGrid.GetChild(currentHp).gameObject);
        activatedSpawns.Remove(index);
    }

    private void AddOneHp()
    {
        if (currentHp >= maxHp) return;
        Instantiate(heartPrefab, heartsGrid);
        currentHp++;
    }

    public static Transform GetSpinPoint(int index)
    {
        return index < activeSpinPoints.Count ? activeSpinPoints[index] : null;
    }
}
