using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed;

    public AudioSource moveSound;
    public AudioSource spinSound;

    public Rigidbody2D rb;
    public Animator anim;

    public SpriteRenderer sr;
    public float timeToSpin;
    public float scorePerSpin = 1;
    public bool isPlaying;
    
    private static readonly int Walk = Animator.StringToHash("Walk");
    private static readonly int Action = Animator.StringToHash("Action");
    private int currentPole = -1;
    private bool isAtPole;
    private bool isWalk;
    private bool action;
    private float horizontal;
    
    private float timeSpinned;
    
    private static readonly int Hurt = Animator.StringToHash("Hurt");
    
    private void Update()
    {
        if (!anim || !sr || !isPlaying) return;
        
        GatherInputs();
        
        SpinningPlate();
        Walking();
    }

    private void GatherInputs()
    {
        horizontal = Input.GetAxis("Horizontal");
        action = Input.GetButton("Fire1") || Input.GetButton("Fire2") || 
                 Input.GetButton("Fire3") || Input.GetButton("Jump");
        
        isAtPole = GameManager.IsPlateActive(currentPole);
        isWalk = horizontal != 0;
    }
    
    private void SpinningPlate()
    {
        anim.SetBool(Action, action && isAtPole);
       
        if (action && isAtPole)
            Spin();
        else if (!action)
            DontSpin();
    }

    private void Spin()
    {
        timeSpinned += Time.deltaTime;
        if(!spinSound.isPlaying && UIEvents.soundIsON)
            spinSound.Play();
            
        UIEvents.OnUpdateScore.Invoke(scorePerSpin * Time.deltaTime);
        
        GameManager.OnResetPlateTime.Invoke(currentPole);
        
        if (!(timeSpinned >= timeToSpin)) return;
        GameManager.OnSpinPlate.Invoke(currentPole);
        timeSpinned = 0;
    }

    private void DontSpin()
    {
        if(spinSound.isPlaying && UIEvents.soundIsON)
            spinSound.Stop();
        timeSpinned = 0;
    }
    
    private void Walking()
    {
        anim.SetBool(Walk, isWalk && (!action || !isAtPole));
        
        if (isWalk && (!action || !isAtPole))
            OnWalk();
        else if (!isWalk)
            DontWalk();
    }

    private void OnWalk()
    {
        if(!moveSound.isPlaying && UIEvents.soundIsON)
            moveSound.Play();
        sr.flipX = horizontal < 0;
    }

    private void DontWalk()
    {
        if(moveSound.isPlaying && UIEvents.soundIsON)
            moveSound.Stop();
    }
    
    private void FixedUpdate()
    {
        if (!rb || anim.GetBool(Hurt)) return;
        if (isWalk && (!action || !isAtPole))
            rb.AddForce(new Vector2(horizontal * moveSpeed, 0), ForceMode2D.Force);
    }
    
    public void IsPlaying(bool toggle)
    {
        isPlaying = toggle;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.transform.CompareTag("PoleTrigger")) return;
        
        var pt = other.GetComponent<PoleTrigger>();
        PoleCheck(pt);
    }

    private void PoleCheck(PoleTrigger pt)
    {
        if (!pt) return;
        currentPole = pt.index;
        isAtPole = GameManager.IsPlateActive(currentPole);
    }
    
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.CompareTag("PoleTrigger"))
            ExitPole();
    }

    private void ExitPole()
    {
        currentPole = -1;
        isAtPole = false;
    }

    public void OnHurt()
    {
        if (UIEvents.soundIsON)
        {
            moveSound.Stop();
            spinSound.Stop();
        }

        rb.velocity = Vector2.zero;
        anim.SetBool(Hurt, true);
    }

    public void StopHurt()
    {
        anim.SetBool(Hurt, false);
    }
}
