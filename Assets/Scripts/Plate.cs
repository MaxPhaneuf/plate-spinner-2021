using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public enum Stage { Dropped, Stable, Fast, Slow, Falling, Fell }
public class Plate : MonoBehaviour
{
    public int index;

    public Animator anim;
    public Collider2D coll;
    public Stage stage;
    public SpriteRenderer sr;
    public AudioSource breakSound;
    public AudioSource throwSound;
    public float timeToNextStage;

    public List<GameObject> brokenPlatesPrefab = new List<GameObject>();
    private float timeSinceLastStage;
    private bool isBroken;

    private static readonly int Fast = Animator.StringToHash("Fast");
    private static readonly int Slow = Animator.StringToHash("Slow");
    private static readonly int Falling = Animator.StringToHash("Falling");

    private int lastBrokenIndex = -1;
    private void Start()
    {
        stage = Stage.Dropped;
    }
    
    private void Update()
    {
        if (stage < Stage.Stable || coll.isTrigger) return;
        if (!(Time.time - timeSinceLastStage >= timeToNextStage)) return;
        
        stage++;
        SetToStage(stage);
        timeSinceLastStage = Time.time;
    }

    public void SpinPlate()
    {
        timeSinceLastStage = Time.time;
    }
    
    public void SpinUp()
    {
        stage--;
        if (stage == Stage.Dropped)
            stage++;
        SetToStage(stage);
        timeSinceLastStage = Time.time;
    }
    
    private void ActivateCollider()
    {
        coll.isTrigger = false;
        timeSinceLastStage = Time.time;
    }

    private void SetToStage(Stage nextStage)
    {
        if (nextStage == Stage.Stable)
            OnStable();
        else if (nextStage == Stage.Fast)
            OnFast();
        else if (nextStage == Stage.Slow)
            OnSlow();
        else if (nextStage == Stage.Falling)
            OnFalling();
        else if (nextStage == Stage.Fell)
            OnFell();
        stage = nextStage;
    }

    private void OnStable()
    {
        anim.SetBool(Fast, false);
        anim.SetBool(Slow, false);
        anim.SetBool(Falling, false);
    }

    private void OnFast()
    {
        anim.SetBool(Fast, true);
        anim.SetBool(Slow, false);
        anim.SetBool(Falling, false);
    }

    private void OnSlow()
    {
        anim.SetBool(Fast, true);
        anim.SetBool(Slow, true);
        anim.SetBool(Falling, false);
    }

    private void OnFalling()
    {
        anim.SetBool(Fast, true);
        anim.SetBool(Slow, true);
        anim.SetBool(Falling, true);
    }

    private void OnFell()
    {
        anim.SetBool(Fast, true);
        anim.SetBool(Slow, true);
        anim.SetBool(Falling, true);
        coll.isTrigger = true;
    }
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.collider.CompareTag("PoleCollider")) return;
        
        stage = Stage.Stable;
        timeSinceLastStage = Time.time;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.CompareTag("Ground"))
            PlateDestroyed();
        else if (other.transform.CompareTag("Player") && !isBroken)
            PlayerBounce();
    }

    private void PlateDestroyed()
    {
        coll.isTrigger = false;
        SetToStage(Stage.Stable);
        isBroken = true;
        
        if (!gameObject) return;
        if(UIEvents.soundIsON)
            breakSound.Play();
        
        sr.enabled = false;
        AddBrokenPlate();
        
        Invoke(nameof(DestroyPlate), breakSound.clip.length);
    }

    private void AddBrokenPlate()
    {
        int broken = Random.Range(0, brokenPlatesPrefab.Count);
        while(broken == lastBrokenIndex) broken = Random.Range(0, brokenPlatesPrefab.Count);
        var t = transform;
        Instantiate(brokenPlatesPrefab[broken], t.position, t.rotation);
        lastBrokenIndex = broken;
    }
    
    private void PlayerBounce()
    {
        var spinPoint = GameManager.GetSpinPoint(index);
        SetToStage(Stage.Fast);
        if(UIEvents.soundIsON)
            throwSound.Play();
        Invoke(nameof(ActivateCollider), 1);
        transform.DOMove(spinPoint.position, 1);
    }
    
    private void DestroyPlate()
    {
        GameManager.OnPlateBroken.Invoke(index);
        Destroy(gameObject);
    }
    
    
}
