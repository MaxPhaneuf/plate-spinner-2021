using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIEvents : MonoBehaviour
{
    public static UnityEvent OnGameOver = new UnityEvent();

    public static UnityEvent OnStartGame = new UnityEvent();
    
    public static UnityEvent<float> OnUpdateScore = new UnityEvent<float>();
    
    public GameObject gameOverPanel;
    public GameObject startPanel;
    public Animator curtainAnim;

    public AudioSource bmg;

    public AudioSource oneUpSound;

    public Toggle musicToggle;
    public Toggle soundToggle;

    public static bool soundIsON;
    public TextMeshProUGUI finalScoreText;
    public TextMeshProUGUI scoreText;

    public float scoreForOneUp = 500;
    private float currentScoreToOneUp = 0;
    private static float currentScore;
    
    private static readonly int Close = Animator.StringToHash("Close");
    private static readonly int Lift = Animator.StringToHash("Lift");

    private void Start()
    {
        OnGameOver.AddListener(GameOver);
        OnUpdateScore.AddListener(UpdateScore);
        OnStartGame.AddListener(StartGame);
    }

    public void StartGame()
    {
        if(musicToggle.isOn)
            bmg.Play();
        soundIsON = soundToggle.isOn;
        curtainAnim.SetBool(Lift, true);
        currentScore = 0;
        scoreText.text = currentScore.ToString("00000000");
        startPanel.SetActive(false);
        gameOverPanel.SetActive(false);
        GameManager.OnStartGame.Invoke();
    }
    
    private void GameOver()
    {
        if(musicToggle.isOn)
            bmg.Stop();
        curtainAnim.SetBool(Lift, false);
        
        finalScoreText.text = currentScore.ToString("00000000");
        gameOverPanel.SetActive(true);
    }
    
    private void UpdateScore(float addedScore)
    {
        currentScore += addedScore;
        currentScoreToOneUp += addedScore;
        if (currentScoreToOneUp >= scoreForOneUp)
        {
            GameManager.OneUp.Invoke();
            if(soundToggle.isOn)
                oneUpSound.Play();
            currentScoreToOneUp = 0;
        }
        scoreText.text = currentScore.ToString("00000000");
    }
}
